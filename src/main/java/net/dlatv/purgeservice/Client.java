package net.dlatv.purgeservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

public class Client {

	public void executeRPC(String path) throws XmlRpcException, UnsupportedEncodingException, IOException {

		XmlRpcClientConfigImpl config;

		try {

			config = new XmlRpcClientConfigImpl();

			try {// https://amc1.lab.mxc.invdes.reduno.com.mx:443/index.php
				config.setServerURL(new URL("https://amc1.lab.mxc.invdes.reduno.com.mx:443/index.php")); // TODO:
																		// Fixear
																		// para
																		// que
																		// siga
																		// redirects
																		// del
																		// URI
																		// que
																		// llega
																		// en
																		// HTTP.
																		// Esto
																		// hardcodeado
																		// no
																		// va.
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			final String username = "backoffice@dlatv.net"; // TODO Pasar a un
															// config
			final String password = "ODLa345$";

			XmlRpcClient client = new XmlRpcClient();

			config.setBasicEncoding("UTF-8");

			System.out.println("URL de servidor: " + config.getServerURL());

			client.setConfig(config);
			HashMap<String, String> paramsMap = new HashMap<String, String>();
			HashMap<String, String> originMap = new HashMap<String, String>();
			
			paramsMap.put("Username", username);
			paramsMap.put("AuthMethod", "password");
			paramsMap.put("AuthString", password);
			
			
			Object[] params = new Object[] { paramsMap, originMap }; //TODO: hay que pasar el origin como mapa, para que quede: 
			
			/*
				<?xml version='1.0'?>
				<methodCall>
				<methodName>ListAll</methodName>
				<params>
				<param>
				<value><struct>
				<member>
				<name>Username</name>
				<value><string>backoffice@dlatv.net</string></value>
				</member>
				<member>
				<name>AuthMethod</name>
				<value><string>password</string></value>
				</member>
				<member>
				<name>AuthString</name>
				<value><string>ODLa345$</string></value>
				</member>
				</struct></value>
				</param>
				<param>
				<value><string>Origin</string></value>
				</param>
				<param>
				<value><struct>
				<member>
				<name>url</name>
				<value><string>http://uspssqro.clarovideo.net</string></value>
				</member>
				</struct></value>
				</param>
				<param>
				<value><array><data>
				<value><string>origin_id</string></value>
				</data></array></value>
				</param>
				</params>
				</methodCall>
			 */

			SSLContext sc = SSLContext.getInstance("SSL");

			try {
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
			} catch (KeyManagementException e) {
				e.printStackTrace();
			}
			System.out.println("El socket Factory es");
			System.out.println(sc.getSocketFactory());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

			String result = (String) client.execute("ListAll", params);
			System.out.println(result);
		} catch (Exception e) {
			System.err.println("JavaClient: " + e);
		}
	}

	TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public void checkClientTrusted(X509Certificate[] certs, String authType) {
			// Trust always
		}

		public void checkServerTrusted(X509Certificate[] certs, String authType) {
			// Trust always

		}
	} };

	// Create empty HostnameVerifier
	HostnameVerifier hv = new HostnameVerifier() {

		public boolean verify(String arg0, SSLSession arg1) {
			return true;
		}
	};
}
