package net.dlatv.purgeservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.apache.xmlrpc.XmlRpcException;

public class App {
	public static void main(String[] args) throws XmlRpcException, UnsupportedEncodingException, IOException,
			KeyManagementException, NoSuchAlgorithmException {

		System.out.println("*****************************************************************************************");
		System.out.println("********************************Inicio el Purge******************************************");
		System.out.println("*****************************************************************************************");

		Client jSoapClient = new Client();

		jSoapClient.executeRPC("/multimediav81/plataforma_vod/TEST/testfile.txt");
	}
}
